<?php

namespace Domain\Entity;

use Exception;

final class Leave
{
    private string $uuid;
    private string $employeeId;
    private string $title;
    private string $dateStart;
    private string $dateEnd;
    private string $comment;
    private string $status;
    private $reason;

    /**
     * Leave constructor.
     * @param string $uuid
     * @param string $employeeId
     * @param string $title
     * @param string $dateStart
     * @param string $dateEnd
     * @param string $comment
     * @param string $status
     * @param null $reason
     */
    private function __construct(
        string $uuid,
        string $employeeId,
        string $title,
        string $dateStart,
        string $dateEnd,
        string $comment,
        string $status = 'pending',
        $reason = null
    )
    {
        $this->uuid       = $uuid;
        $this->employeeId = $employeeId;
        $this->title      = $title;
        $this->dateStart  = $dateStart;
        $this->dateEnd    = $dateEnd;
        $this->comment    = $comment;
        $this->status     = $status;
        $this->reason     = $reason;
    }

    /**
     * Leave constructor ( Static factory Pattern ).
     * @param $uuid
     * @param $employeeId
     * @param $title
     * @param $dateStart
     * @param $dateEnd
     * @param $comment
     * @param string $status
     * @param null $reason
     * @return Leave
     * @throws Exception
     */
    public static final function buildMakeLeave(
        $uuid,
        $employeeId,
        $title,
        $dateStart,
        $dateEnd,
        $comment,
        string $status = 'pending',
        $reason = null
    ): Leave
    {
        if(empty($uuid))
            throw new Exception('Leave id required');

        if(empty($employeeId))
            throw new Exception('Leave Employee id required');

        if(empty($title))
            throw new Exception('Leave Title required');

        if(empty($dateStart))
            throw new Exception('Leave Start Date required');

        if(empty($dateEnd))
            throw new Exception('Leave End Date required');

        if(empty($comment))
            throw new Exception('Leave Comment required');

        return new Leave(
            $uuid,
            $employeeId,
            htmlentities($title, ENT_QUOTES, 'UTF-8'),
            $dateStart,
            $dateEnd,
            htmlentities($comment, ENT_QUOTES, 'UTF-8'),
            $status,
            htmlentities($reason, ENT_QUOTES, 'UTF-8'),
        );

    }

    /**
     * Approve the current Leave Request
     * @return Leave
     * @throws Exception
     */
    public function accept(): Leave
    {
        if($this->status === 'approved')
            throw new Exception('Leave Already approved');

        if($this->status === 'rejected')
            throw new Exception('You cant accept a rejected Leave');

        return new Leave(
            $this->getUuid(),
            $this->getEmployeeId(),
            $this->getTitle(),
            $this->getDateStart(),
            $this->getDateEnd(),
            $this->getComment(),
            'approved',
            $this->getReason()
        );
    }

    /**
     * Reject the current Leave Request
     * @return Leave
     * @throws Exception
     */
    public function reject()
    {
        if($this->status === 'approved')
            throw new Exception('Leave Already approved');

        if($this->status === 'rejected')
            throw new Exception('Leave Already rejected');


        return new Leave(
            $this->getUuid(),
            $this->getEmployeeId(),
            $this->getTitle(),
            $this->getDateStart(),
            $this->getDateEnd(),
            $this->getComment(),
            'rejected'
        );
    }

    /**
     * @return mixed
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @return mixed
     */
    public function getEmployeeId()
    {
        return $this->employeeId;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * @return mixed
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * @return mixed
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @return mixed|null
     */
    public function getReason()
    {
        return $this->reason;
    }

    public function getValues(): array
    {
        return [
            'uuid'       => self::getUuid(),
            'status'     => self::getStatus(),
            'date_start' => self::getDateStart(),
            'date_end'   => self::getDateEnd(),
            'title'      => self::getTitle(),
            'comment'    => self::getComment(),
            'reason'     => self::getReason(),
        ];
    }

}