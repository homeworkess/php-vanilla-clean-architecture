<?php

namespace Domain\Exception;

use Exception;

final class ExistingPendingLeaveRequest extends Exception {}