<?php

namespace Domain\UseCase;

use Common\Utils\Debug;
use Domain\Entity\Leave;
use Domain\Exception\EmployeeNotFound;
use Domain\Repository\EmployeeRepositoryInterface;
use Domain\Repository\LeaveRepositoryInterface;
use Exception;

class EmployeePlaceLeaveRequestUseCase
{
    /**
     * @var $employeeRepository EmployeeRepositoryInterface
     * @var $leaveRepository LeaveRepositoryInterface
     */
    private $employeeRepository;
    private $leaveRepository;

    /**
     * LeaveRequestUseCase constructor.
     * @param $employeeRepository
     * @param $leaveRepository
     */
    public function __construct($leaveRepository, $employeeRepository)
    {
        $this->employeeRepository = $employeeRepository;
        $this->leaveRepository = $leaveRepository;
    }

    /**
     * Execute the employee leave request
     * @Todo should received a requestObject instead
     * @param $request array
     * @return array
     * @throws EmployeeNotFound
     * @throws Exception
     */
    public function execute(array $request): array
    {
        // Retrieve the existing Employee
        $existingEmployee = $this->employeeRepository->findByUUID($request['employee_id'], true);

        // The employee should be present on the system
        if(!$existingEmployee)
            throw new EmployeeNotFound("The requested employee doesn't exists");

        // Build an immutable leave entity instance
        $leaveRequest = Leave::buildMakeLeave(
            uniqid(),
            $existingEmployee->getUuid(),
            $request['title'],
            $request['date_start'],
            $request['date_end'],
            $request['comment']
        );

        /**
         * Persist the Entity state through the persistence layer
         * @todo Is the employee allowed to place more than one Leave
         */
        $this->leaveRepository->placeLeaveRequest([
            'employeeId' => $existingEmployee->getUuid(),
            'uuid'       => $leaveRequest->getUuid(),
            'title'      => $leaveRequest->getTitle(),
            'date_start' => $leaveRequest->getDateStart(),
            'date_end'   => $leaveRequest->getDateEnd(),
            'comment'    => $leaveRequest->getComment(),
            'status'     => $leaveRequest->getStatus()
        ]);

        /**
         *  Return a DTO for the UI
         * @todo Refactor should be a ResponseModel
         */
        return [
            'uuid' => $leaveRequest->getUuid(),
            'status' => $leaveRequest->getStatus()
        ];

    }

}