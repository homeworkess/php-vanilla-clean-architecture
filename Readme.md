# Aragon Api 

Step #1 - change the database credentials in index.php :
```bash
const DB_HOST = '127.0.0.1';
const DB_NAME = 'aragon_api';
const DB_USER = 'root';
const DB_PASS = 'root';
```

Step #2 - Import the database dump file  :

```bash
aragon_api.sql
```

Step #3 - Fire the php internal dev server with the following command :

```bash
php -S localhost:8080
```

### Routes

Create a leave request :

```bash
[POST]
http://localhost:8080/api/leaves

[body]
{
    "employee_id": "3cd1d7ec-bd82-11eb-8529-0242ac130003",
    "title": "Leave request #1",
    "date_start": "25/05/2021",
    "date_end": "30/05/2021",
    "comment": "Leave Request #1 comment"
}
```

List leave requests :

```bash
[GET]
http://localhost:8080/api/leaves/index/?user_id=3cd1d7ec-bd82-11eb-8529-0242ac130003
```

Approve / Reject a Leave Request ( Business rules are included in th comments inside the use case) :

```bash
[PATCH]
http://localhost:8080/api/leaves

[body]
{
    "managerUuid": "02c24e3f-ab14-4c60-99ad-1cc3d16544a8",
    "leaveUuid": "3cd1d7ec-bd82-11eb-8529-0242ac130003",
    "status": "approved",
    "reason": "Reason content"
}

```

List all the employees :

```bash
[GET]
http://localhost:8080/api/employees
```