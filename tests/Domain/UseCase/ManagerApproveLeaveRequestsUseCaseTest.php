<?php

namespace App\Tests\UseCase;


use Common\Utils\Debug;
use Domain\Exception\EmployeeIsNotManager;
use Domain\Exception\EmployeeNotFound;
use Domain\RequestModel\LeaveApprovalRequestModelInterface;
use Domain\UseCase\ManagerApproveLeaveRequestsUseCase;
use Infrastructure\Repository\EmployeeRepository;
use Infrastructure\Repository\LeaveRepository;
use PHPUnit\Framework\TestCase;

class ManagerApproveLeaveRequestsUseCaseTest extends TestCase
{

    protected function setUp(): void
    {
        parent::setUp();
    }

    /**
     * @test
     * @throws EmployeeIsNotManager
     */
    public function can_approve_a_leave()
    {
        $this->expectException(InvalidArgumentException::class);
        // Leave Repository
        $leaveRepository = $this->createMock(LeaveRepository::class);

        // Employee Repository
        $employeeRepository = $this->createMock(EmployeeRepository::class);
        $employeeRepository->method('findByUUID')
            ->willReturn([]);


        $approvalRequestModel = $this->createMock(LeaveApprovalRequestModelInterface::class);
        $approvalRequestModel->method('getEmployeeId')
            ->willReturn('xxxxx-xxxxxx-xxxxxx');


        // Repository Config
        $leaveRepository->method('findByUUID')
            ->willReturn([]);

        // UseCase
        $useCase = new ManagerApproveLeaveRequestsUseCase(
            $leaveRepository,
            $employeeRepository
        );

        $this->expectException(EmployeeNotFound::class);

        $useCase->execute($approvalRequestModel);
    }
}