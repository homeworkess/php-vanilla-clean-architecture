<?php


namespace Domain\ResponseModel;


interface ResponseModelInterface
{
    function getData();
}