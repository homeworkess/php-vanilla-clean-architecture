<?php

namespace UserInterface\Controller;

use Common\Utils\Debug;
use Domain\Repository\LeaveRepositoryInterface;
use Infrastructure\Repository\PDOLeaveRepository;
use Psr\Container\ContainerInterface;

class DefaultController extends ApplicationController
{

    function index()
    {
        Debug::dd($this->container);
    }

    function add()
    {
        // TODO: Implement add() method.
    }

    function view($id)
    {
        // TODO: Implement view() method.
    }

    function delete($id)
    {
        // TODO: Implement delete() method.
    }

    function patch()
    {
        // TODO: Implement patch() method.
    }
}