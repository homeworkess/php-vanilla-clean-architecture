<?php

namespace App\Tests\Repository;

use Common\Utils\Debug;
use Domain\Repository\GatewayInterface;
use Exception;
use Infrastructure\Repository\EmployeeRepository;
use PDO;
use PHPUnit\Framework\TestCase;



class PDOGatewayImpl implements GatewayInterface
{

    public function getInstance(): PDO
    {
        return new PDO('sqlite::memory:');
    }
}


class EmployeeRepositoryTest extends TestCase
{



    public function test_shouldReturnAnInstanceOfGateWay()
    {
        $pdo = new PDOGatewayImpl();
        $repository = new EmployeeRepository($pdo);
        $this->assertNotNull($repository->getGateway());
    }




}