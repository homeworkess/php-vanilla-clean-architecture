<?php

namespace UserInterface\Controller;

use Common\Utils\Debug;
use Domain\Repository\LeaveRepositoryInterface;
use Psr\Container\ContainerInterface;

abstract class ApplicationController
{
    protected ContainerInterface $container;

    public function __construct(
        ContainerInterface $container
    )
    {
        $this->container = $container;
    }

    /**
     * @param $data array
     * @param bool $success
     * @param string $format
     */
    protected final function render(
        array $data,
        bool $success = true,
        string $format = 'json'
    )
    {
        if ($format === 'json') {
            header('Content-Type: application/json');
            echo json_encode([
                'success' => $success,
                'data' => $data
            ]);
            exit();
        }

        echo $data;
    }

    /**
     * @param $message
     */
    protected final function renderError($message)
    {
        header('Content-Type: application/json');
        echo json_encode([
            'success' => false,
            'error' => $message
        ]);
        exit();
    }


    /**
     * Retrieve the Global post data
     * @Todo Return a RequestObject instead
     * @return string | null
     */
    protected final function getInputData(): ?string
    {
        return file_get_contents('php://input');
    }

    /**
     * Retrieve the Global post data
     * @Todo Return a RequestObject instead
     * @return array | null
     */
    protected final function getQueryData(): ?array
    {
        return $_GET;
    }

    /**
     * @return bool
     */
    protected final function isPostRequest(): bool
    {
        return $_SERVER['REQUEST_METHOD'] === 'POST';
    }

    /**
     * @return bool
     */
    protected final function isAjaxRequest(): bool
    {
        return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest';
    }

}