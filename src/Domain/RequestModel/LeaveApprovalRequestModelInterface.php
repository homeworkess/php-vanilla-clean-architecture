<?php

namespace Domain\RequestModel;

interface LeaveApprovalRequestModelInterface
{


    function getLeaveId();

    function getEmployeeId();

    function getStatus();

}