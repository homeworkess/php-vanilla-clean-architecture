<?php

namespace Domain\Exception;

final class EmployeeNotFound extends \Exception{}