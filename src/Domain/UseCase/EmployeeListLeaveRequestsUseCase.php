<?php

namespace Domain\UseCase;

use DI\Annotation\Inject;
use DI\Annotation\Injectable;
use Domain\Exception\EmployeeNotFound;
use Domain\Repository\EmployeeRepositoryInterface;
use Domain\Repository\LeaveRepositoryInterface;
use Exception;

/**
 * Class EmployeeListLeaveRequestsUseCase
 * @package Domain\UseCase
 * @Injectable(lazy=true)
 */
class EmployeeListLeaveRequestsUseCase
{
    /**
     * @var $employeeRepository EmployeeRepositoryInterface
     */
    private EmployeeRepositoryInterface $employeeRepository;
    private LeaveRepositoryInterface $leaveRepository;

    /**
     * LeaveRequestUseCase constructor.
     * @param LeaveRepositoryInterface $leaveRepository
     * @param EmployeeRepositoryInterface $employeeRepository
     * @Inject
     */
    public function __construct(
        LeaveRepositoryInterface $leaveRepository,
        EmployeeRepositoryInterface $employeeRepository
    )
    {
        $this->employeeRepository = $employeeRepository;
        $this->leaveRepository = $leaveRepository;
    }

    /**
     * Execute the employee leave request
     * @Todo should received a requestObject instead
     * @param $request array
     * @return array
     * @throws EmployeeNotFound
     * @throws Exception
     */
    public function execute(array $request): array
    {

        // Retrieve the existing Employee
        $existingEmployee = $this->employeeRepository->findByUUID($request['employeeId']);

        // The employee should be present on the system
        if (!$existingEmployee)
            throw new EmployeeNotFound("The requested employee doesn't exists");

        // Is the current employee a manager ?
        if ($existingEmployee->isManager()) {

            $leaves = $this->leaveRepository
                ->findByManagerUuid($existingEmployee->getUuid());

        } else {

            $leaves = $this->leaveRepository
                ->findByEmployeeUUID($existingEmployee->getUuid());

        }

        /**
         *  Return a DTO for the UI
         * @todo Refactor should be a ResponseModel
         */
        return array_map(fn($leave) => $leave->getValues(), $leaves);

    }

}