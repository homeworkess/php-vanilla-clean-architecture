<?php

namespace Common\Utils;

class Debug
{
    public static function dd($data)
    {
        echo '<pre>';
        print_r($data);
        echo '</pre>';
        exit;
    }
}