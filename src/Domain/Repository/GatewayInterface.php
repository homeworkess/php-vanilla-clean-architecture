<?php

namespace Domain\Repository;

interface GatewayInterface
{
    public function getInstance();
}