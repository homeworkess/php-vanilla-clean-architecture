<?php

use Common\Utils\Debug as _;
use DI\Container;
use DI\ContainerBuilder;
use Domain\Repository\EmployeeRepositoryInterface;
use Domain\Repository\LeaveRepositoryInterface;
use Infrastructure\Repository\PDOEmployeeRepository;
use Infrastructure\Repository\PDOLeaveRepository;
use Infrastructure\Router\Router;
use UserInterface\Controller\DefaultController;

require __DIR__ . '/vendor/autoload.php';


try {

    $containerBuilder = new ContainerBuilder();
    // $containerBuilder->useAutowiring(true);
    $containerBuilder->useAnnotations(true);
    $containerBuilder->addDefinitions('config.php');
    $container = $containerBuilder->build();

    // Build the necessary Global variables $_GET, $_POST, $_SERVER
    $router = Infrastructure\Router\Router::createFromGlobals();
    $router::setContainer($container);

    // Debug

    // Initialise the controller & the relative action / method
    $router::route();

} catch (Exception $e) {



}



