<?php

namespace Domain\Entity;

use Common\Utils\Debug;
use Exception;

class Employee
{
    /**
     * @var string $uuid
     * @var string $firstName
     * @var string $LastName
     * @var string $groupId
     * @var string $managerId
     */
    private string $uuid;
    private string $firstName;
    private string $LastName;
    private string $groupId;
    private ?string $managerId = null;
    private $leavesRequests;

    /**
     * Employee constructor.
     * @param string $uuid
     * @param string $firstName
     * @param string $LastName
     * @param string $groupId
     * @param string $managerId
     * @param $leavesRequests
     */
    private function __construct(
        string $uuid,
        string $firstName,
        string $LastName,
        string $groupId,
        string $managerId,
        $leavesRequests
    )
    {
        $this->uuid           = $uuid;
        $this->firstName      = $firstName;
        $this->LastName       = $LastName;
        $this->groupId        = $groupId;
        $this->managerId      = $managerId;
        $this->leavesRequests = $leavesRequests;
    }

    /**
     * @throws Exception
     */
    public static function buildMakeEmployee(
        $uuid,
        $firstName,
        $LastName,
        $groupId = null,
        $managerId = null,
        $leavesRequests = []
    ): Employee
    {

        if(empty($uuid))
            throw new Exception('uuid is required');

        if(empty($firstName))
            throw new Exception('First name is required');

        if(empty($firstName))
            throw new Exception('First name is required');

        if(empty($LastName))
            throw new Exception('Last name is required');

        if(empty($groupId))
            throw new Exception('GroupId is required');

        return new Employee(
            $uuid,
            $firstName,
            $LastName,
            $groupId,
            $managerId,
            $leavesRequests
        );

    }

    /**
     * @param Leave $leaveRequest
     * @return Employee
     * @throws Exception
     */
    public function addLeaveRequest(Leave $leaveRequest): Employee
    {
        if(empty($leaveRequest))
            throw new Exception('You cannot add an empty leave Request');

        //@todo check for a presence of a leave request into the same period

        // Add the new request
        $leavesRequests = $this->leavesRequests;
        array_push($leavesRequests, $leaveRequest);

        return new Employee(
            $this->getUuid(),
            $this->getFirstName(),
            $this->getLastName(),
            $this->getGroupId(),
            $this->getManagerId(),
            $leavesRequests
        );

    }

    /**
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->LastName;
    }

    /**
     * @return string
     */
    public function getGroupId(): string
    {
        return $this->groupId;
    }

    /**
     * @return string
     */
    public function getManagerId(): string
    {
        return $this->managerId;
    }

    /**
     * @return mixed
     */
    public function getLeavesRequests()
    {
        return $this->leavesRequests;
    }

    /**
     * Check if the current employee is a manager
     * @return bool
     */
    public function isManager(): bool
    {
        return $this->getGroupId() === '2342-11eb';
    }

}