<?php

namespace Infrastructure\Router;

use Common\Utils\Debug;
use Psr\Container\ContainerInterface;
use UserInterface\Controller\DefaultController;

class Router
{
    private static $get;
    private static $post;
    private static $cookie;
    private static $files;
    private static $server;
    private static ContainerInterface $container;

    public static function createFromGlobals(): Router
    {

        self::$get = $_GET;
        self::$post = $_POST;
        self::$cookie = $_COOKIE;
        self::$files = $_FILES;
        self::$server = $_SERVER;

        return new self();
    }

    public static function setContainer($container)
    {
        self::$container = $container;
    }

    /**
     * @return mixed
     */
    public static function getQuery($key = null)
    {
        return $key ? self::$get[$key] : self::$get;
    }

    /**
     * @return mixed
     */
    public static function getPost()
    {
        return self::$post;
    }

    /**
     * @return mixed
     */
    public static function getCookie()
    {
        return self::$cookie;
    }

    /**
     * @return mixed
     */
    public static function getFiles()
    {
        return self::$files;
    }

    /**
     * @return mixed
     */
    public static function getServer()
    {
        return self::$server;
    }

    /**
     * @return mixed
     */
    public static function getMethod()
    {
        return self::$server['REQUEST_METHOD'];
    }



    /**
     * Build a controller instance based on the provided URI information
     * @return DefaultController|mixed
     */
    public static function route()
    {
        $uriSegments = explode('/', self::getServer()['REQUEST_URI']);

        $requestedControllerName = isset($uriSegments[2]) ? ucfirst($uriSegments[2]) . 'Controller' : 'DefaultController';

        $controllerFullNameWithNameSpace = "UserInterface\\Controller\\$requestedControllerName";

        // Check the controller presence
        if(!class_exists($controllerFullNameWithNameSpace))
            die("Controller Class => $controllerFullNameWithNameSpace not found");

        // Initialise the controller
        $controller = self::$container->get($controllerFullNameWithNameSpace);

        // Controller Action
        $action = 'index';

        switch (self::getMethod()){
            case "GET":
                $action = "index";
                break;

            case "POST":
                $action = "add";
                break;

            case "PATCH":
                $action = "patch";
                break;
        }

        ob_start();
            echo  $controller->{$action}();
        ob_end_flush();


    }


}