<?php

namespace UserInterface\Controller;

use Domain\UseCase\EmployeeListUseCase;
use Exception;
use Infrastructure\Repository\EmployeeRepository;

class EmployeesController extends ApplicationController
{
    /**
     * List all Employees
     * @Route('/', name='employees_list')
     * @throws Exception
     */
    public function index()
    {
        try {

            $employeeListUseCase = new EmployeeListUseCase(new EmployeeRepository());
            $this->render($employeeListUseCase->execute());

        } catch (Exception $e) {

            $this->renderError($e->getMessage());

        }

    }

    function add()
    {
        // TODO: Implement add() method.
    }

    function view($id)
    {
        // TODO: Implement view() method.
    }

    function delete($id)
    {
        // TODO: Implement delete() method.
    }

    function patch()
    {
        // TODO: Implement patch() method.
    }
}