<?php

namespace Infrastructure\Repository;

use Common\Utils\DateFormatter;
use Common\Utils\Debug;
use Domain\Entity\Leave;
use Domain\Repository\EmployeeRepositoryInterface;
use Domain\Repository\EntityNotFoundException;
use Domain\Repository\LeaveRepositoryInterface;
use Exception;


class PDOLeaveRepository implements LeaveRepositoryInterface
{
    private \PDO $db;
    private EmployeeRepositoryInterface $employeeRepository;

    /**
     * LeaveRepository constructor.
     */
    public function __construct(\PDO $pdo, EmployeeRepositoryInterface $employeeRepository)
    {
        $this->db = $pdo;
        $this->employeeRepository = $employeeRepository;
    }

    /**
     * @param $data
     * @return Leave
     * @throws Exception
     */
    private function buildLeaveEntity($data): Leave
    {
        return Leave::buildMakeLeave(
            $data['l_uuid'],
            $data['e_uuid'],
            $data['l_title'],
            $data['l_date_start'],
            $data['l_date_end'],
            $data['l_comment'],
            $data['l_status'],
            $data['l_reason']
        );
    }

    /**
     * Fetch Leaves by employeeID
     * @param $uuid
     * @return array | null
     * @throws Exception
     */
    public function findByEmployeeUUID($uuid): ?array
    {
        // Fetch current manager employees
        $employee = $this->employeeRepository->findByUUID($uuid);

        $db = $this->db;
        $sqlQuery = "

             SELECT 
                 l.uuid as l_uuid, 
                 l.title as l_title, 
                 l.status as l_status, 
                 DATE_FORMAT(l.date_start, '%d-%m-%Y') as l_date_start,
                 DATE_FORMAT(l.date_end, '%d-%m-%Y') as l_date_end,
                 l.comment as l_comment,
                 l.reason as l_reason
             FROM leaves AS l
             JOIN employees AS e ON l.employee_id = e.id
             WHERE e.uuid = :e_uuid

        ";

        $query = $db->prepare($sqlQuery);
        $query->execute(['e_uuid' => $employee->getUuid()]);
        $result = $query->fetchAll(\PDO::FETCH_ASSOC);


        // Build Domain Entities list
        return array_map(function ($leave) use ($employee) {
            return $this->buildLeaveEntity(
                array_merge($leave, [
                    'e_uuid' => $employee->getUuid()
                ])
            );
        }, $result);


    }

    /**
     * Fetch all Leaves
     * @return array | null
     * @throws Exception
     */
    public function findByManagerUuid($managerUuid, $hydrate = false)
    {
        // Fetch current manager employees
        $managerEmployees = $this->employeeRepository->findByManagerUuid($managerUuid, $hydrate);

        // Extract & format the employees ID's
        $managerEmployeesIds = array_map(function ($employee) {
            return $employee['id'];
        }, $managerEmployees);

        // Manager should have at least one employee
        if (empty($managerEmployeesIds))
            throw new Exception('Current manager has no employees');

        $db = $this->db;
        $sqlQuery = "

             SELECT 
                 l.uuid as l_uuid, 
                 l.title as l_title, 
                 l.status as l_status, 
                 DATE_FORMAT(l.date_start, '%d-%m-%Y') as l_date_start,
                 DATE_FORMAT(l.date_end, '%d-%m-%Y') as l_date_end,
                 l.comment as l_comment,
                 l.reason as l_reason,
                 e.uuid as e_uuid,
                 e.id as e_id
             FROM leaves AS l
             JOIN employees as e
             ON l.employee_id = e.id
             WHERE e.id IN (:ids)

        ";

        $query = $db->prepare($sqlQuery);
        $query->execute(['ids' => implode(',', $managerEmployeesIds)]);

        $result = $query->fetchAll(\PDO::FETCH_ASSOC);

        if ($hydrate) {

            // Build Domain Entities list
            return array_map(function ($leave) use ($result) {

                return Leave::buildMakeLeave(
                    $leave['l_uuid'],
                    $leave['e_uuid'],
                    $leave['l_title'],
                    $leave['l_date_start'],
                    $leave['l_date_end'],
                    $leave['l_comment'],
                    $leave['l_status'],
                    $leave['l_reason']
                );

            }, $result);

        }

        return $result;
    }

    /**
     * @param Leave $data
     * @return Leave | null
     * @throws Exception
     */
    function placeLeaveRequest(Leave $data): ?Leave
    {

        try {

            $db = $this->db;
            $query = $db->prepare("INSERT INTO leaves (employee_id, uuid, title, comment, status, date_start, date_end, created, modified) VALUES (?,?,?,?,?,?,?,?,?)");

            $employee = $this->employeeRepository->findByUUID($data['employeeId'], false);

            // Format to MySQL
            $dateStart = DateFormatter::format($data['date_start']);
            $dateEnd = DateFormatter::format($data['date_end']);
            $created = $modified = DateFormatter::format('now', 'Y-m-d H:i:s');

            // _::dd($created);

            $result = $query->execute([
                $employee['id'],
                $employee['uuid'],
                $data['title'],
                $data['comment'],
                $data['status'],
                $dateStart,
                $dateEnd,
                $created,
                $modified
            ]);

            // Rebuild the Domain entity
            if ($result) {

                return Leave::buildMakeLeave(
                    $data['uuid'],
                    $employee['uuid'],
                    $data['title'],
                    $dateStart,
                    $dateEnd,
                    $data['comment']
                );

            }

        } catch (\PDOException $e) {
        }


    }

    /**
     * @param Leave $data
     * @return bool
     * @throws Exception
     * @todo refactor transaction
     */
    function update($data)
    {
        // Fetch previous Leave entity
        $leave = $this->findByUUID($data->getUuid());

        try {

            $db = $this->db;
            $sqlQuery = "
                UPDATE leaves
                SET 
                    employee_id = :employee_id,
                    uuid = :uuid,
                    title = :title,
                    comment = :comment,
                    status = :status,
                    date_start = :date_start,
                    date_end = :date_end,
                    modified = :modified
                WHERE
                    uuid = :id
            
            ";
            $query = $db->prepare($sqlQuery);

            // Format to MySQL
            $modified = DateFormatter::format('now', 'Y-m-d H:i:s');

            return $query->execute([
                    'id'          => $data->getUuid(),
                    'employee_id' => $leave['e_id'],
                    'uuid'        => $leave['l_uuid'],
                    'title'       => $leave['l_title'],
                    'comment'     => $leave['l_comment'],
                    'status'      => $data->getStatus(),
                    'date_start'  => DateFormatter::format($leave['l_date_start']),
                    'date_end'    => DateFormatter::format($leave['l_date_end']),
                    'modified'    => $modified
            ]);

        } catch (\PDOException $e) {}

    }

    /**
     * Fetch single Leave
     * @param $uuid
     * @return array | Leave
     * @throws Exception
     */
    function findByUUID($uuid): ?Leave
    {
        $sqlQuery  = "SELECT ";
        $sqlQuery .= "l.uuid as l_uuid, ";
        $sqlQuery .= "l.title as l_title, ";
        $sqlQuery .= "l.status as l_status, ";
        $sqlQuery .= "DATE_FORMAT(l.date_start, '%d-%m-%Y') as l_date_start, ";
        $sqlQuery .= "DATE_FORMAT(l.date_end, '%d-%m-%Y') as l_date_end, ";
        $sqlQuery .= "l.comment as l_comment, ";
        $sqlQuery .= "l.reason as l_reason, ";
        $sqlQuery .= "e.uuid as e_uuid, ";
        $sqlQuery .= "e.id as e_id ";
        $sqlQuery .= "FROM leaves AS l ";
        $sqlQuery .= "JOIN employees as e ";
        $sqlQuery .= "ON l.employee_id = e.id ";
        $sqlQuery .= "WHERE l.uuid = :uuid" ;

        $query = $this->db->prepare($sqlQuery);
        $query->execute([':uuid' => $uuid]);
        $result = $query->fetch(\PDO::FETCH_ASSOC);

        if(empty($result))
            throw new EntityNotFoundException("Entity $uuid not found");

        return Leave::buildMakeLeave(
            $result['l_uuid'],
            $result['e_uuid'],
            $result['l_title'],
            $result['l_date_start'],
            $result['l_date_end'],
            $result['l_comment'],
            $result['l_status'],
            $result['l_reason']
        );

    }

    function findByEmployeeUuids(array $ids): ?Leave
    {
        // TODO: Implement findByEmployeeUuids() method.
    }
}