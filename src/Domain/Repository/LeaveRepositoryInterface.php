<?php

namespace Domain\Repository;

use Domain\Entity\Leave;

interface LeaveRepositoryInterface
{

    /**
     * @param $uuid
     * @return array | null
     */
    function findByUUID($uuid): ?Leave;

    /**
     * @param Leave $data array
     * @return Leave
     */
    function placeLeaveRequest(Leave $data): ?Leave;

    /**
     * @param array $ids
     * @return Leave[]|null
     */
    function findByEmployeeUuids(array $ids):? Leave;

}