<?php

namespace Domain\Exception;

final class EmployeeIsNotManager extends \Exception{}