<?php

namespace Domain\Repository;

use Domain\Entity\Employee;

interface EmployeeRepositoryInterface
{

    /**
     * @param int $uuid
     * @return array | null
     */
    public function findByUUID(int $uuid): ?Employee;

}