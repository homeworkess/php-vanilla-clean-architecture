<?php

namespace UserInterface\Controller;

use Common\Utils\Debug;
use DI\Annotation\Inject;
use Domain\UseCase\EmployeeListLeaveRequestsUseCase;
use Domain\UseCase\EmployeePlaceLeaveRequestUseCase;
use Domain\UseCase\ManagerApproveLeaveRequestsUseCase;
use Exception;

class LeavesController extends ApplicationController
{
    /**
     * List all Leaves
     *
     * @Inject
     * @throws Exception
     */
    public function index(EmployeeListLeaveRequestsUseCase $useCase)
    {
        $data = $this->getQueryData();

        if (empty($data['user_id']))
            $this->renderError('User uuid required');

        try {

            $this->render($useCase->execute([
                'employeeId' => $data['user_id']
            ]));

        } catch (Exception $e) {

            $this->renderError($e->getMessage());

        }

    }


    /**
     * Append a new Leave
     * @Route('/add', name='leave_add')
     */
    public function add()
    {
        if ($this->isPostRequest()) {

            $data = json_decode($this->getInputData(), true);

            try {

                $leaveRepository = new LeaveRepository();
                $employeeRepository = new EmployeeRepository();

                $leaveRequestUseCase = new EmployeePlaceLeaveRequestUseCase($leaveRepository, $employeeRepository);
                $response = $leaveRequestUseCase->execute($data);

                $this->render($response);


            } catch (Exception $e) {

                $this->renderError($e->getMessage());

            }

        }

        die('<p>Only Post request allowed</p>');

    }


    public function patch()
    {
        $bodyData  = json_decode($this->getInputData(), true);

        // @todo refactor RequestObject validation
        if(empty($bodyData['managerUuid']))
            $this->renderError('managerUid parameter is required');

        if(empty($bodyData['leaveUuid']))
            $this->renderError('leaveUid parameter is required');

        if(empty($bodyData['status']))
            $this->renderError('status parameter is required');

        if(empty($bodyData['reason']))
            $this->renderError('reason parameter is required');

        try {

            $leaveRepository = new LeaveRepository();
            $employeeRepository = new EmployeeRepository();

            $managerApproveLeaveRequestUseCase = new ManagerApproveLeaveRequestsUseCase($leaveRepository, $employeeRepository);
            $response = $managerApproveLeaveRequestUseCase->execute([
                'managerUuid' => $bodyData['managerUuid'],
                'leaveUuid'   => $bodyData['leaveUuid'],
                'status'     => $bodyData['status'],
                'reason'     => $bodyData['reason'],
            ]);

            $this->render($response);

        } catch (Exception $e) {

            $this->renderError($e->getMessage());

        }
    }

    function view($id = null)
    {
        // TODO: Implement view() method.
    }

    function delete($id = null)
    {
        // TODO: Implement delete() method.
    }
}