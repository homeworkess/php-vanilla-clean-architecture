<?php


namespace Common\Utils;


use Exception;

class DateFormatter
{
    /**
     * @param $date
     * @param string $format
     * @return string
     * @throws Exception
     */
    public static function format($date, $format = 'Y-m-d')
    {
        $dateTime = new \DateTime(str_replace('/', '-', $date));
        return $dateTime->format($format);
    }
}