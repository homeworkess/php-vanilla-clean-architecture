<?php

namespace Domain\UseCase;

use Domain\Entity\Leave;
use Domain\Exception\EmployeeIsNotManager;
use Domain\Exception\EmployeeNotFound;
use Domain\Repository\EmployeeRepositoryInterface;
use Domain\RequestModel\LeaveApprovalRequestModelInterface;
use Domain\ResponseModel\LeaveApprovalResponseModelInterface;
use Exception;
use Infrastructure\Repository\LeaveRepository;

class ManagerApproveLeaveRequestsUseCase
{
    /**
     * @var $employeeRepository EmployeeRepositoryInterface
     * @var LeaveRepository $leaveRepository
     */
    private $employeeRepository;
    private $leaveRepository;

    /**
     * LeaveRequestUseCase constructor.
     * @param $employeeRepository
     * @param $leaveRepository
     */
    public function __construct($leaveRepository, $employeeRepository)
    {
        $this->employeeRepository = $employeeRepository;
        $this->leaveRepository = $leaveRepository;
    }

    /**
     * Execute the employee leave request
     * @Todo should received a requestObject instead
     * @param LeaveApprovalResponseModelInterface $request
     * @return array
     * @throws EmployeeIsNotManager
     * @throws EmployeeNotFound
     * @throws Exception
     */
    public function execute(LeaveApprovalRequestModelInterface $request): array
    {
        // Retrieve the existing Employee
        $exists = $this->employeeRepository
            ->findByUUID(
                $request->getEmployeeId(),
                false
            );

        // The Manager should be present on the system
        if (!$exists)
            throw new EmployeeNotFound("The requested employee doesn't exists");

        // The Employee must be a manager
        if (!$exists->isManager())
            throw new EmployeeIsNotManager("The current employee is not a manager");

        // Retrieve the Leave
        /** @var Leave $leave */
        $leave = $this->leaveRepository->findByUUID($request['leaveUuid'], true);

        // Current leave employee
        $leaveEmployee = $this->employeeRepository->findByUUID($leave->getUuid(), true);

        // The Manager should be responsible for the leave validation / rejection
        if ($leaveEmployee->getManagerId() !== $exists->getUuid())
            throw new EmployeeIsNotManager("The current manager is not allowed to update the leave request");

        // Validate Provided status
        if($request['status'] === 'approved')
            $leave = $leave->accept();

        if($request['status'] === 'rejected')
            $leave = $leave->reject();

        // Rebuild the entity ( Immutable )
        $leave = Leave::buildMakeLeave(
          $leave->getUuid(),
            $leave->getEmployeeId(),
            $leave->getTitle(),
            $leave->getDateStart(),
            $leave->getDateEnd(),
            $leave->getComment(),
            $leave->getStatus(),
            $request['reason']
        );

        // Persist the new entity state
        $result = $this->leaveRepository->update($leave);

        if(!$result)
            throw new Exception('An issue has occurred during entity persistence');

        /**
         *  Return a DTO for the UI
         * @todo Refactor should be a ResponseModel
         */
        return [
            'uuid'   => $leave->getUuid(),
            'status' => $leave->getStatus(),
            'reason' => $leave->getReason(),
        ];

    }

}