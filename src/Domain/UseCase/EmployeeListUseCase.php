<?php

namespace Domain\UseCase;

use Common\Utils\Debug;
use Domain\Entity\Employee;
use Domain\Entity\Leave;
use Domain\Exception\EmployeeNotFound;
use Domain\Repository\EmployeeRepositoryInterface;
use Domain\Repository\LeaveRepositoryInterface;
use Exception;
use Infrastructure\Repository\LeaveRepository;

class EmployeeListUseCase
{
    /**
     * @var $employeeRepository EmployeeRepositoryInterface
     */
    private $employeeRepository;

    /**
     * LeaveRequestUseCase constructor.
     * @param $employeeRepository
     */
    public function __construct($employeeRepository)
    {
        $this->employeeRepository = $employeeRepository;
    }

    /**
     * List all the employees
     * @Todo should received a requestObject instead
     * @throws Exception
     */
    public function execute(): array
    {
        /** @var Employee[] $employees */
        $employees = $this->employeeRepository->findAll(true);

        /**
         *  Return a DTO for the UI
         * @todo Refactor should be a ResponseModel
         */
        return array_map(function($employee){

            return [
                'uuid'       => $employee->getUuid(),
                'first_name' => $employee->getFirstName(),
                'last_name'  => $employee->getLastName(),
                'is_manager' => $employee->isManager()
            ];

        }, $employees);

    }

}