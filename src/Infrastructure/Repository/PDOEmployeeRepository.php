<?php

namespace Infrastructure\Repository;

use Domain\Entity\Employee;
use Domain\Repository\EmployeeRepositoryInterface;
use Exception;
use PDO;


class PDOEmployeeRepository implements EmployeeRepositoryInterface
{
    private PDO $db;

    /**
     * LeaveRepository constructor.
     */
    public function __construct(PDO $pdo)
    {
        $this->db = $pdo;
    }

    /**
     * Fetch a group by primary key
     * @param string $groupId
     * @return array|null
     */
    private function findByGroupId(string $groupId): ?array
    {
        $db = $this->db;
        $query = $db->prepare("SELECT * FROM groups AS g WHERE g.id = :id");
        $query->execute([':id' => $groupId]);
        return $query->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * Fetch an employee by primary key
     * @param string $Id
     * @return array|null
     */
    private function findById($Id)
    {
        $db = $this->db;
        $query = $db->prepare("SELECT * FROM employees AS e WHERE e.id = :id");
        $query->execute([':id' => $Id]);
        return $query->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * Fetch a single Employee by UUID
     * @param $uuid
     * @return Employee|null
     * @throws Exception
     */
    public function findByUUID($uuid): ?Employee
    {

        try {

            $query = $this->db->prepare("SELECT * FROM employees AS e WHERE e.uuid = :uuid");
            $query->execute([':uuid' => $uuid]);
            $result = $query->fetch(PDO::FETCH_ASSOC);

            $manager = null;
            $group = $this->findByGroupId($result['group_id']);

            // Got a manager ?
            if($result['manager_id'])
                $manager = $this->findById($result['manager_id']);

            return Employee::buildMakeEmployee(
                $result['uuid'],
                $result['first_name'],
                $result['last_name'],
                $group['uuid'],
                !empty($manager) ? $manager['uuid'] : null
            );

        } catch (\PDOException $e){

        }
    }

    /**
     * Find the current manager employees
     * @param $uuid
     * @param false $hydrate
     * @return array | null
     */
    public function findByManagerUuid($uuid, $hydrate = false)
    {
        $db = $this->db;
        $query = $db->prepare("SELECT e.* FROM employees AS e JOIN employees as m ON e.manager_id = m.id WHERE m.uuid = :uuid");
        $query->execute([':uuid' => $uuid]);
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Fetch all employees
     * @param bool $hydrate
     * @return array|Employee|mixed|null
     * @throws Exception
     */
    public function findAll(bool $hydrate = false)
    {

        try {

            $db = $this->db;
            $sqlQuery = "
                SELECT 
                    e.uuid as e_uuid,
                    e.first_name as e_first_name,
                    e.last_name as e_last_name,
                    m.uuid as m_uuid,
                    g.uuid as g_uuid   
                
                FROM employees as e
                LEFT JOIN employees as m ON e.manager_id = m.id
                JOIN groups as g ON e.group_id = g.id
                   
            ";
            $query = $db->prepare($sqlQuery);
            $query->execute();
            $result = $query->fetchAll(PDO::FETCH_ASSOC);

            // Rebuild the Domain entity or simple array
            if($hydrate){

                return array_map(function($employee){

                    return Employee::buildMakeEmployee(
                        $employee['e_uuid'],
                        $employee['e_first_name'],
                        $employee['e_last_name'],
                        $employee['g_uuid'],
                        $employee['m_uuid']
                    );
                }, $result);

            }

            return $result;

        } catch (\PDOException $e){

        }
    }

}