<?php


use Domain\Repository\EmployeeRepositoryInterface;
use Domain\Repository\LeaveRepositoryInterface;
use Domain\UseCase\EmployeeListLeaveRequestsUseCase;
use Infrastructure\Repository\PDOEmployeeRepository;

use Infrastructure\Repository\PDOLeaveRepository;
use Psr\Container\ContainerInterface;
use UserInterface\Controller\DefaultController;
use function DI\create;
use function DI\get;

return [

    /** Database config */
    'database.host'     => 'localhost',
    'database.name'     => 'aragon_api',
    'database.user'     => 'root',
    'database.password' => 'root',
    'database.options'  => null,

    /** Database */
    PDO::class => static function
    (ContainerInterface $c){

        $dsn = "mysql:host=" . $c->get('database.host') .
               ";dbname=" . $c->get('database.name');

        return new PDO(
            $dsn,
            $c->get('database.user'),
            $c->get('database.password'),
            $c->get('database.options')
        );
    },


    /** Repositories */
    EmployeeRepositoryInterface::class => function
    (ContainerInterface $c) {
        return new PDOEmployeeRepository($c->get(PDO::class));
    },

    LeaveRepositoryInterface::class => function
    (ContainerInterface $c) {
        return new PDOLeaveRepository(
            $c->get(PDO::class),
            $c->get(EmployeeRepositoryInterface::class),
        );
    },

    /** UseCases */
    EmployeeListLeaveRequestsUseCase::class => function
    (ContainerInterface $c) {
        return new EmployeeListLeaveRequestsUseCase(
            $c->get(LeaveRepositoryInterface::class),
            $c->get(EmployeeRepositoryInterface::class)
        );
    },


    /** Controllers */


    /** Helpers */

];

